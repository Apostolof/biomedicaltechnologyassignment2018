# Biomedical Technology Assignment 2018, AUTH
> Spike sorting of filtered LFP recordings dataset using machine learning

## Table of Contents

- [Clone](#clone)
- [Execution](#execution)
- [Support](#support)
- [License](#license)

---

## Clone

Clone this repo to your local machine using `https://gitlab.com/Apostolof/biomedicaltechnologyassignment2018.git`

---

## Execution

Scripts were written and tested in Matlab R2016a (v9.0.0.341360). Any newer version of the software should also be able to execute the scripts without problems.

---

## Support

Reach out to us:

- [apostolof's email](mailto:apotwohd@gmail.com "apotwohd@gmail.com")
- [charaldp's email](mailto:charaldp@ece.auth.gr "charaldp@ece.auth.gr")

---

## License

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/Apostolof/biomedicaltechnologyassignment2018/blob/master/LICENSE)